'use strict'

module.exports.register = function ({ config }) {
  const logger = this.getLogger('@djencks/antora-timer')
  config.logLevel && (logger.level = config.logLevel)
  const startTime = Date.now()
  let lastTime = startTime

  ;[
    'playbookBuilt',
    'beforeProcess',
    'contentAggregated',
    'uiLoaded',
    'contentClassified',
    'documentsConverted',
    'navigationBuilt',
    'pagesComposed',
    'siteMapped',
    'redirectsProduced',
    'beforePublish',
    // 'sitePublished',
  ].forEach((stage) => {
    this.prependListener(stage, () => stageTime(stage))
    this.on(stage, () => postStageTime(stage))
  })

  this.prependListener('sitePublished',
    () => stageTime('sitePublished')
  )

  this.on('sitePublished',
    () => {
      stageTime('sitePublished')
      logger.info(`total build time ${(Date.now() - startTime) / 1000}s`)
    }
  )

  function stageTime (stage) {
    const now = Date.now()
    logger.info(`stage ${stage} took ${(now - lastTime) / 1000}s`)
    lastTime = now
  }

  function postStageTime (stage) {
    const now = Date.now()
    logger.info(`Extensions after stage ${stage} took ${(now - lastTime) / 1000}s`)
    lastTime = now
  }
}
