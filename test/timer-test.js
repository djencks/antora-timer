/* eslint-env mocha */
'use strict'

const { expect } = require('chai')
const EventEmitter = require('events')
const timer = require('./../lib/timer')

describe('timer tests', () => {
  let eventEmitter
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    timer.prependListener = (event, code) => {
      eventEmitter.prependListener(event, code)
    }
    timer.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    timer.getLogger = (name) => {
      return {
        debug: (msg) => { log.push(msg) },
        info: (msg) => { log.push(msg) },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('basic test', () => {
    timer.register({ config: { logLevel: 'debug' } })
    expect(eventEmitter.eventNames().length).to.equal(12)
    eventEmitter.eventNames().forEach((name) => {
      expect(eventEmitter.listenerCount(name)).to.equal(2)
    })
  })
})
