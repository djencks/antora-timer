# Antora Timer Extension
:version: 0.0.1-beta.1

This is an Antora extension that times the Antora stages and logs the result.

See the more complete README.adoc at gitlab.
